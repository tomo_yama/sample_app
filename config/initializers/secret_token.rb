# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
SampleApp::Application.config.secret_key_base = 'ef7585ba8b17d4dc439d5d6dad26884440cc004b89a0d1a8296dfeb2305382764168ee6cef7715364a28ccf551ed0a8ab7215a41f1d4e0ea10f04752c0bb360f'
